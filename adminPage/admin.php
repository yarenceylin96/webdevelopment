<?php

session_start();

    //connect to database  address  username  pw    database
    $conn = new mysqli("localhost", "root", "", "homework");
    // check connection

    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

$_SESSION['message'] = "Admin Login Only";

if(isset($_POST['username']) && isset($_POST['password'])){
    $username = $_POST['username'];
    $password = $_POST['password'];


    $user_check_query = "SELECT * FROM `admin` WHERE AdminName='$username' OR AdminPass='$password'";
    $result = mysqli_query($conn, $user_check_query);
    $user = mysqli_fetch_assoc($result);
    if ($user) { // if user exists
        if ($user['AdminName'] == $username && $user['AdminPass'] ==  $password) {
            $_SESSION['message'] = "Admin Login Successful";
        }
    }
    else{
        $_SESSION['message'] = "DENIED ACCESS";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Page</title>
    <style>
        html {
            height: 100%;
        }
    body {
    text-align: center;
    font-family: sans-serif;
    text-align: center;
    background-image: linear-gradient(to bottom right, rgb(82, 185, 216), rgb(14, 16, 121));
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
}
#username, #pw, #pw2, #email{
    display: inline-flex;
    margin: 1%;
    width: 71%;
    height: 40px;
}
#signinBtn {
    margin: 1%;
    display: inline-block;
    width: 25%;
    height: 53px;
    line-height: 19px;
    text-align: center;
}

.loginForm {
    top: 50%;
    position: absolute;
    left: 50%;
    transform: translate(-50%,-50%);
    box-shadow: 0 0 20px 3px black;
    background-color: #fffcfc26;
}

div#message {
    font-weight: 200;
    padding-bottom: 15px;
    margin-bottom: 20px;
    color: #fbfbfb;
    box-shadow: 0px 0px 9px 0px black;
    margin-top: 2px;
    text-align: center;
    padding-top: 10px;
    background-color: #2185da;
    font-size: larger;
}

</style>


</head>
<body>
    <div class = "loginForm">
    <div id = "message"><?= $_SESSION['message'] ?></div>
        <form action="admin.php" method="POST">
                <input type="text" name="username" id="username" placeholder="Admin Account Name" required="required">
                <input type="password" name="password" id="pw" placeholder="Admin Pass" required="required">
                <button type="submit" id="signinBtn">Sign In</button>
        </form>
    </div> 
</body>
</html>


